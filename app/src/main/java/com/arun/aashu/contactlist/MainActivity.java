package com.arun.aashu.contactlist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    EditText ed1,ed2,ed3;
    Button b1,b2;
    ArrayList<String> arList=new ArrayList<String>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ed1=findViewById(R.id.ed1);
        ed2=findViewById(R.id.ed2);
        ed3=findViewById(R.id.ed3);
        b1=findViewById(R.id.buttonadd);
        b2=findViewById(R.id.buttonshow);



        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name=ed1.getText().toString();
                String phone=ed2.getText().toString();
                String address=ed3.getText().toString();

                arList.add("Name:- "+name+"\nphone:- "+phone+"\naddress:- "+address);


                ed1.setText("");
                ed2.setText("");
                ed3.setText("");

            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(MainActivity.this,Main2Activity.class);
                intent.putStringArrayListExtra("list",arList);
                startActivityForResult(intent,0);

            }
        });



    }
    
    @Override
protected  void onActivityResult(int requestcode ,int resultCode,Intent data){

        if(requestcode==0 && resultCode==420 && data!=null){
                int p=data.getExtras().getInt("position");

                arList.remove(p);
        }
    }



}
