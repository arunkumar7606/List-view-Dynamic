package com.arun.aashu.contactlist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;


public class Main2Activity extends AppCompatActivity {

    ListView l1;

    ArrayAdapter<String> ad;
    ArrayList<String> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        l1=findViewById(R.id.listview1);

        Bundle b=getIntent().getExtras();
        arrayList=b.getStringArrayList("list");

        ad=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,arrayList);

        l1.setAdapter(ad);


        l1.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {

                arrayList.remove(position);
                ad.notifyDataSetChanged();

                Intent intent=new Intent(Main2Activity.this,MainActivity.class);
                intent.putExtra("postion",position);
                setResult(420,intent);

                return false;
            }
        });






    }
}
